package cmd

import (
	"fmt"
	"log"
	"strconv"

	"github.com/spf13/cobra"
	queue "gitlab.com/opsgem-public/queue/pkg/queue"
	queueTypes "gitlab.com/opsgem-public/queue/pkg/types"
	"gopkg.in/yaml.v2"
)

var getMessageCommand = &cobra.Command{
	Use:   "get [queue]",
	Short: "Get a message",
	Long:  `Get a message to a queue.`,
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		client := queue.New()

		var timeout int
		var err error
		var message queueTypes.Message

		if len(args) == 2 {
			timeout, err = strconv.Atoi(args[1])
			if err != nil {
				log.Println(err)
				return
			}
			message, err = client.GetMessageTimeout(args[0], timeout)
			if err != nil {
				log.Println(err)
				return
			}
		} else {
			message, err = client.GetMessage(args[0])
			if err != nil {
				log.Println(err)
				return
			}
		}
		err = message.Working()
		if err != nil {
			log.Println(err)
			return
		}

		y, err := yaml.Marshal(message)
		if err != nil {
			log.Println(err)
			return
		}
		fmt.Println(string(y))
	},
}

func init() {
	RootCmd.AddCommand(getMessageCommand)
}
