package cmd

import (
	"log"

	"github.com/spf13/cobra"
	queue "gitlab.com/opsgem-public/queue/pkg/queue"
	queueTypes "gitlab.com/opsgem-public/queue/pkg/types"
)

var publishCommand = &cobra.Command{
	Use:   "publish [queue] [message]",
	Short: "Publish a message",
	Long:  `Publish a message to a queue.`,
	Args:  cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		client := queue.New()
		message := queueTypes.Message{
			Queue:     args[0],
			Payload:   args[1],
			OnSuccess: "results",
		}
		id, err := client.Publish(&message)
		if err != nil {
			log.Println(err)
			return
		}
		log.Println(id)
	},
}

func init() {
	RootCmd.AddCommand(publishCommand)
}
