package main

import (
	cmd "gitlab.com/opsgem-public/queue/cmd/myapp"
)

var (
	version string
)

func main() {
	cmd.Execute()
}
