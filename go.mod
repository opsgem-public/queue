module gitlab.com/opsgem-public/queue

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-redis/redis/v8 v8.0.0-beta.9
	github.com/google/uuid v1.1.2
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	gopkg.in/yaml.v2 v2.2.7
)
