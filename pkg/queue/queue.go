package queue

import (
	"time"

	redisQueueStore "gitlab.com/opsgem-public/queue/pkg/store/redis"
	queueTypes "gitlab.com/opsgem-public/queue/pkg/types"
)

type (
	Queue struct {
		store queueTypes.Store
	}
)

func New() *Queue {
	return &Queue{
		store: redisQueueStore.NewDefault(),
	}
}

func NewRedis(host, pass string, db int) *Queue {
	return &Queue{
		store: redisQueueStore.New(host, pass, db),
	}
}

func NewRedisWithUser(host, user, pass string, db int) *Queue {
	return &Queue{
		store: redisQueueStore.NewWithUser(host, user, pass, db),
	}
}

func (q *Queue) Publish(message *queueTypes.Message) (string, error) {
	message.Created = time.Now()
	message.Updated = time.Now()

	return q.store.Publish(message)
}

func (q *Queue) GetMessage(queue string) (queueTypes.Message, error) {
	return q.store.GetMessage(queue)
}

func (q *Queue) GetMessageTimeout(queue string, timeout int) (queueTypes.Message, error) {
	return q.store.GetMessageTimeout(queue, timeout)
}

// Key / Value Functions
func (q *Queue) KeyGet(key string, value interface{}) (err error) {
	return q.store.KeyGet(key, value)
}

func (q *Queue) KeySet(key string, value interface{}) (err error) {
	return q.store.KeySet(key, value)
}

func (q *Queue) KeySetP(key string, value interface{}, ttl int) (err error) {
	return q.store.KeySetP(key, value, ttl)
}

func (q *Queue) KeyLock(key string) (token string, err error) {
	return q.store.KeyLock(key)
}

func (q *Queue) KeyLockP(key string, ttl int) (token string, err error) {
	return q.store.KeyLockP(key, ttl)
}

func (q *Queue) KeyLockGet(key string, value interface{}) (token string, err error) {
	return q.store.KeyLockGet(key, value)
}

func (q *Queue) KeyLockGetP(key string, value interface{}, ttl int) (token string, err error) {
	return q.store.KeyLockGetP(key, value, ttl)
}

func (q *Queue) KeyHaslock(key string, token string) (err error) {
	return q.store.KeyHaslock(key, token)
}

func (q *Queue) KeyUnLock(key string, token string) (err error) {
	return q.store.KeyUnLock(key, token)
}

func (q *Queue) KeyUnLockSet(key string, value interface{}, token string) (err error) {
	return q.store.KeyUnLockSet(key, value, token)
}
