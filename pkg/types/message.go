package queueTypes

import "time"

type (
	Message struct {
		Id         string      `json:"id"`
		Store      Store       `json:"-"`
		Payload    interface{} `json:"payload"`
		Queue      string      `json:"queue"`
		TTL        int         `json:"ttl"`
		OnError    string      `json:"onerror"`
		OnTimeout  string      `json:"ontimeout"`
		OnSuccess  string      `json:"onsuccess"`
		Retries    int         `json:"retries"`
		MaxRetries int         `json:"maxretries"`
		Status     string      `json:"status"`
		Error      string      `json:"error"`
		Created    time.Time   `json:"created"`
		Updated    time.Time   `json:"updated"`
	}
)

func (m *Message) SetStatus(status string) error {
	m.Status = status
	m.Updated = time.Now()

	return m.Store.SetMessageStatus(m)
}

func (m *Message) Working() error {
	m.Status = "working"
	m.Updated = time.Now()

	return m.Store.SetMessageStatus(m)
}

func (m *Message) Success() error {
	m.Status = "success"
	m.Updated = time.Now()

	return m.Store.SetMessageStatus(m)
}

func (m *Message) Failed() error {
	m.Status = "failed"
	m.Updated = time.Now()

	if m.Error == "" {
		m.Error = "Unknown failure"
	}

	return m.Store.SetMessageStatus(m)
}

func (m *Message) Timeout() error {
	m.Status = "timeout"
	m.Updated = time.Now()

	if m.Error == "" {
		m.Error = "Timeout"
	}

	return m.Store.SetMessageStatus(m)
}

// func (m *Message) ReQueue() error {
// 	_, err := m.Store.ReQueue(m)
// 	return err
// }
