package queueTypes

type (
	Store interface {
		// Queue
		Publish(message *Message) (string, error)
		GetMessage(queue string) (Message, error)
		GetMessageTimeout(queue string, timeout int) (Message, error)
		SetMessageStatus(message *Message) error
		// ListQueues()
		// GetQueueItems()
		// GetQueueDepth()

		// Stream

		// Key / Value
		KeyGet(key string, value interface{}) (err error)
		KeySet(key string, value interface{}) (err error)
		KeySetP(key string, value interface{}, ttl int) (err error)
		KeyLock(key string) (token string, err error)
		KeyLockP(key string, ttl int) (token string, err error)
		KeyLockGet(key string, value interface{}) (token string, err error)
		KeyLockGetP(key string, value interface{}, ttl int) (token string, err error)
		KeyHaslock(key string, token string) (err error)
		KeyUnLock(key string, token string) (err error)
		KeyUnLockSet(key string, value interface{}, token string) (err error)
		//KeySetTtl(jey string, ttl int) (err error)
	}
)
