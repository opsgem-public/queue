package redisQueueStore

import (
	"encoding/json"
	"errors"
	"strings"
	"time"

	"github.com/google/uuid"
)

func (s *RedisQueueStore) KeyGet(key string, value interface{}) (err error) {
	p := s.rdb.Get(*s.ctx,
		strings.Join(
			[]string{
				s.prefix,
				"kv",
				key},
			s.seperator),
	)

	if p.Err() != nil {
		return p.Err()
	}

	return json.Unmarshal([]byte(p.Val()), value)
}

func (s *RedisQueueStore) KeySet(key string, value interface{}) (err error) {
	return s.KeySetP(key, value, 0)
}

func (s *RedisQueueStore) KeySetP(key string, value interface{}, ttl int) (err error) {
	data, err := json.Marshal(value)
	if err != nil {
		return err
	}

	return s.rdb.Set(*s.ctx,
		strings.Join(
			[]string{
				s.prefix,
				"kv",
				key},
			s.seperator),
		data, time.Duration(ttl)).Err()
}

func (s *RedisQueueStore) KeyLock(key string) (token string, err error) {
	return s.KeyLockP(key, 0)
}

func (s *RedisQueueStore) KeyLockP(key string, ttl int) (token string, err error) {
	token = uuid.New().String()
	p := s.rdb.SetNX(*s.ctx,
		strings.Join(
			[]string{
				s.prefix,
				"kv", key, "lock"},
			s.seperator),
		token, time.Duration(ttl))
	if p.Err() != nil {
		return "", err
	}
	if p.Val() {
		return token, nil
	}
	return "", errors.New("Failed to obtain lock.")
}

func (s *RedisQueueStore) KeyLockGet(key string, value interface{}) (token string, err error) {
	return s.KeyLockGetP(key, value, 0)
}

func (s *RedisQueueStore) KeyLockGetP(key string, value interface{}, ttl int) (token string, err error) {
	token, err = s.KeyLockP(key, ttl)
	if err != nil {
		return
	}

	err = s.KeyGet(key, value)

	return
}

func (s *RedisQueueStore) KeyHaslock(key string, token string) (err error) {
	p := s.rdb.Get(*s.ctx,
		strings.Join(
			[]string{
				s.prefix,
				"kv",
				key,
				"lock"},
			s.seperator),
	)

	if p.Err() != nil {
		return p.Err()
	}

	if p.Val() != token {
		err = errors.New("Tokens don't match.")
		return
	}

	return
}

func (s *RedisQueueStore) KeyUnLock(key string, token string) (err error) {
	err = s.KeyHaslock(key, token)
	if err != nil {
		return
	}

	err = s.rdb.Del(*s.ctx, strings.Join([]string{s.prefix, "kv", key, "lock"}, s.seperator)).Err()

	return
}

func (s *RedisQueueStore) KeyUnLockSet(key string, value interface{}, token string) (err error) {
	err = s.KeyHaslock(key, token)
	if err != nil {
		return
	}

	err = s.KeySet(key, value)
	if err != nil {
		s.KeyUnLock(key, token)
		return
	}

	err = s.KeyUnLock(key, token)

	return
}
