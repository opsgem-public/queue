package redisQueueStore

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/google/uuid"
	"github.com/spf13/viper"

	queueTypes "gitlab.com/opsgem-public/queue/pkg/types"
)

type (
	RedisQueueStore struct {
		rdb       *redis.Client
		ctx       *context.Context
		prefix    string
		seperator string
	}
)

func NewDefault() *RedisQueueStore {
	var host = ""
	var pass = ""
	var db = 0

	return New(host, pass, db)
}

func New(host, pass string, db int) *RedisQueueStore {
	var user = ""

	return NewWithUser(host, user, pass, db)

}

func NewWithUser(host, user, pass string, db int) *RedisQueueStore {
	var prefix = "queue"

	viper.SetEnvPrefix("QUEUE")
	viper.AutomaticEnv() // read in environment variables that match

	viper.SetDefault("REDIS_HOST", host)
	viper.SetDefault("REDIS_USER", user)
	viper.SetDefault("REDIS_PASS", pass)
	viper.SetDefault("REDIS_DB", db)
	viper.SetDefault("REDIS_TLS_ENABLED", false)

	var ctx = context.Background()

	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
	}

	if !viper.GetBool("REDIS_TLS_ENABLED") {
		tlsConfig = nil
	}

	rdb := redis.NewClient(&redis.Options{
		Username:  viper.GetString("REDIS_USER"),
		Addr:      viper.GetString("REDIS_HOST"),
		Password:  viper.GetString("REDIS_PASS"),
		DB:        viper.GetInt("REDIS_DB"),
		TLSConfig: tlsConfig,
	})

	return &RedisQueueStore{
		rdb:       rdb,
		ctx:       &ctx,
		prefix:    prefix,
		seperator: ":",
	}
}

func (s *RedisQueueStore) SetPrefix(prefix string) {
	s.prefix = prefix
}

func (s *RedisQueueStore) SetSeperator(seperator string) {
	s.seperator = seperator
}

func (s *RedisQueueStore) Publish(message *queueTypes.Message) (string, error) {
	message.Id = uuid.New().String()
	message.Status = "queued"

	err := s.setMessageProcessingStatus(message, 0)
	if err != nil {
		return message.Id, err
	}

	err = s.lpush("queues"+s.seperator+message.Queue, message)

	return message.Id, err
}

func (s *RedisQueueStore) GetMessage(queue string) (message queueTypes.Message, err error) {
	data, err := s.blpop("queues"+s.seperator+queue, 0)
	if err != nil {
		return message, err
	}

	if len(data) != 2 {
		return message, errors.New("No messages returned.")
	}
	err = json.Unmarshal([]byte(data[1]), &message)

	message.Store = s

	return message, nil
}

func (s *RedisQueueStore) GetMessageTimeout(queue string, timeout int) (message queueTypes.Message, err error) {
	data, err := s.blpop("queues"+s.seperator+queue, time.Duration(timeout)*time.Millisecond)
	if err != nil {
		return message, err
	}

	if len(data) != 2 {
		return message, errors.New("No messages returned.")
	}
	err = json.Unmarshal([]byte(data[1]), &message)

	message.Store = s

	return message, nil
}

func (s *RedisQueueStore) SetMessageStatus(message *queueTypes.Message) (err error) {
	switch message.Status {
	case "working":
		err = s.setMessageProcessingStatus(message, 0)
	case "success":
		err = s.delMessageProcessingStatus(message.Id)
		err = s.reQueueMessage(message.OnSuccess, message)
	case "failed":
		err = s.delMessageProcessingStatus(message.Id)
		err = s.reQueueMessage(message.OnError, message)
	case "timeout":
		err = s.delMessageProcessingStatus(message.Id)
		err = s.reQueueMessage(message.OnTimeout, message)
	default:
		err = s.setMessageProcessingStatus(message, 0)
	}
	return
}

func (s *RedisQueueStore) reQueueMessage(queue string, message *queueTypes.Message) error {
	if queue == "" {
		return nil
	}

	return s.lpush("queues"+s.seperator+queue, message)
}

func (s *RedisQueueStore) setMessageProcessingStatus(message *queueTypes.Message, ttl int) error {
	return s.setKey("processing"+s.seperator+message.Id, message, time.Duration(ttl)*time.Second)
}

func (s *RedisQueueStore) delMessageProcessingStatus(id string) error {
	return s.delKey("processing" + s.seperator + id)
}

func (s *RedisQueueStore) getMessageProcessing(id string) (*queueTypes.Message, error) {
	message := queueTypes.Message{}

	err := s.getKey("processing"+s.seperator+id, &message)

	message.Store = s

	return &message, err
}

func (s *RedisQueueStore) setKey(key string, value interface{}, ttl time.Duration) error {
	data, err := json.Marshal(value)
	if err != nil {
		return err
	}

	return s.rdb.Set(*s.ctx,
		strings.Join(
			[]string{
				s.prefix,
				key},
			s.seperator),
		data, ttl).Err()
}

func (s *RedisQueueStore) getKey(key string, value interface{}) error {
	p := s.rdb.Get(*s.ctx,
		strings.Join(
			[]string{
				s.prefix,
				key},
			s.seperator),
	)

	if p.Err() != nil {
		return p.Err()
	}

	return json.Unmarshal([]byte(p.Val()), value)
}

func (s *RedisQueueStore) delKey(key string) error {
	p := s.rdb.Del(*s.ctx,
		strings.Join(
			[]string{
				s.prefix,
				key},
			s.seperator),
	)

	return p.Err()
}

func (s *RedisQueueStore) lpush(key string, value interface{}) error {
	data, err := json.Marshal(value)
	if err != nil {
		return err
	}
	return s.rdb.LPush(
		*s.ctx,
		strings.Join(
			[]string{
				s.prefix,
				key},
			s.seperator),
		string(data)).Err()
}

func (s *RedisQueueStore) blpop(key string, ttl time.Duration) ([]string, error) {
	return s.rdb.BRPop(
		*s.ctx,
		ttl,
		strings.Join(
			[]string{
				s.prefix,
				key},
			s.seperator),
	).Result()
}
